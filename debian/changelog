jcc (3.6-1) UNRELEASED; urgency=medium

  [ Emmanuel Arias ]
  * New upstream version 3.6
  * New maintainer (Closes: #922568):
    - Add myself to Maintainer field on d/control.
  * Bump debhelper to 12 (from 9.2):
    - Update debhelper to debhelper-compat on d/control Build-Depends.
  * Update Vcs-* repository to salsa repository.
  * Add myself on debian/* files copyright on d/copyright.
  * Bump Standard-Version to 4.4.1
  * d/control: add autopkgtest-pkg-python
  * Remove Python2 support.
  * d/control: Remove X-Python-Version for Python 2
  * d/salsa-ci.yml: enable salsa-ci.
  * d/gbp.conf: add gbp.conf file.

  [Ludovico Cavedon]
  * Fix jdk path with Java 9 (Closes: #875579).
  * Fall-back to client-mode JVM is the server-mode one is not available
    (Closes: #885955).
  * Refresh patches.
  * Expand the "default-jdk" symlink at build time, so the run-time
    dependency is on the correct JDK version.
  * Make sure to depend on the JRE package we built against. Do not link
    against libjava as it is not necessary.
  * Change priority extra to optional.
  * No longer remove JCC.egg-info/SOURCES.txt during clean.
  * Add lintian-overrides for JRE rpath (needed because libjvm.so does not
    have an SONAME).
  * Build library with hardening=+all hardening flags.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Remove unnecessary X-Python{,3}-Version field in debian/control.
  * Use secure URI in Homepage field.
  * Rely on pre-initialized dpkg-architecture variables.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Sat, 15 Feb 2020 23:19:15 -0300

jcc (2.22-1) unstable; urgency=medium

  * Imported Upstream version 2.22 (Closes: #838263).
  * Update ppc64el-stacksize.patch to fix FTBFS on powerpc, thanks to
    Dmitry Nezhevenko (Closes: #833857).
  * Add gnukfreebsd.patch to fix FTBFS on kfreebsd, thanks to
    Dmitry Nezhevenko (Closes: #834102).
  * Refresh patches.
  * Update Standards-Version to 3.9.8.
  * Add build-dependency on dh-python.
  * Remove reference to invalid python:Breaks variable.
  * Remove no longer needed binary-or-shlib-defines-rpath lintian override.
  * Disable automatic building jcc-dbgsym for now, as _jcc.so is built without
    .debug section (despite -g being present).

 -- Ludovico Cavedon <cavedon@debian.org>  Wed, 07 Dec 2016 00:52:54 -0800

jcc (2.21-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * d/rules: Fix build on ppc64el by changing JAVAARCH variable to ppc64le.
    Thanks to Fernando Seiti Furusato for the report and patch.
    (Closes: #799713)
  * Build-depend on default-jdk instead of openjdk-7-jdk.
    Depend on default-jdk | java7-sdk for jcc binary package.
    Thanks to Matthias Klose for the report. (Closes: #814165)
  * d/rules: Use default-java instead of a specific JDK version.
  * d/control: Use https and cgit.
  * Ensure that the package can be built twice in a row by removing
    JCC.egg-info/SOURCES.txt in dh_auto_clean override too.

 -- Markus Koschany <apo@debian.org>  Mon, 21 Mar 2016 19:53:15 +0100

jcc (2.21-1) unstable; urgency=low

  [ Fernando Seiti Furusato ]
  * Increase JVM stack size on ppc64el (Closes: #768064, LP: #1371761).

  [ Ludovico Cavedon ]
  * Imported Upstream version 2.21
  * Update watch file to use pypi.debian.net

 -- Ludovico Cavedon <cavedon@debian.org>  Sun, 20 Sep 2015 15:29:59 -0700

jcc (2.20-1) unstable; urgency=medium

  * Imported Upstream version 2.20.
  * Fix java arch directory on arm64 and ppc64el, thanks to Matthias Klose
    <doko@ubuntu.com> and Fernando Seiti Furusato <ferseiti@br.ibm.com>
    (Closes: #754099).
  * Update Standards-Version to 3.9.6.

 -- Ludovico Cavedon <cavedon@debian.org>  Sun, 21 Sep 2014 12:19:26 -0700

jcc (2.17-1) unstable; urgency=low

  * Imported Upstream version 2.17
  * Refresh avoid-ftbfs-unknown-archs.patch.
  * Update Standards-Version to 3.9.4.
  * Change Vcs fields to canonical format.

 -- Ludovico Cavedon <cavedon@debian.org>  Sat, 21 Sep 2013 02:10:36 -0700

jcc (2.13-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Transition package to use openjdk-7:
    - d/control: BD on openjdk-7-jdk, switch runtime dependency
      to openjdk-7-jre.
    - d/rules: Switch openjdk-6 paths to openjdk-7 equivalents.
    Thanks to James Page (Closes: #684384)

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 13 Jun 2013 09:22:28 +0200

jcc (2.13-1) unstable; urgency=low

  * Imported Upstream version 2.13
  * Update Standrads-Version to 3.9.3.
  * Use debhelper compat 9.
  * Update copyright file to machine-readable format 1.0.

 -- Ludovico Cavedon <cavedon@debian.org>  Mon, 25 Jun 2012 00:27:38 -0700

jcc (2.12-1) unstable; urgency=low

  * Imported Upstream version 2.12

 -- Ludovico Cavedon <cavedon@debian.org>  Sun, 01 Jan 2012 00:52:12 +0100

jcc (2.11-3) unstable; urgency=low

  * Link against libjvm.so (LP: #848931, thanks to James Page @ Ubuntu).

 -- Ludovico Cavedon <cavedon@debian.org>  Sat, 29 Oct 2011 15:57:10 -0700

jcc (2.11-2) unstable; urgency=low

  * Fix arch-dependent paths in rules file (thanks to Nobuhiro Iwamatsu,
    Closes: #644585).

 -- Ludovico Cavedon <cavedon@debian.org>  Sun, 09 Oct 2011 14:50:26 -0700

jcc (2.11-1) unstable; urgency=low

  * New upstream release.
  * Change paths to openjdk libraries (for openjdk >= 6b23~pre9-1~,
    Closes: #642814).

 -- Ludovico Cavedon <cavedon@debian.org>  Tue, 27 Sep 2011 23:51:41 -0700

jcc (2.9-1) unstable; urgency=low

  * New upstream release.
  * Update Standards-Version to 3.9.2.

 -- Ludovico Cavedon <cavedon@debian.org>  Thu, 07 Jul 2011 22:03:05 -0700

jcc (2.8-1) unstable; urgency=low

  * New upstream release.

 -- Ludovico Cavedon <cavedon@debian.org>  Thu, 14 Apr 2011 23:53:06 -0700

jcc (2.7-2) unstable; urgency=low

  * Upload to unstable.
  * Drop cdbs and switch to debhelper 8 and dh_python2.

 -- Ludovico Cavedon <cavedon@debian.org>  Sat, 02 Apr 2011 15:03:16 -0700

jcc (2.7-1) experimental; urgency=low

  * New upstream release.
  * Update Standards-Version to 3.9.1.

 -- Ludovico Cavedon <cavedon@debian.org>  Fri, 17 Dec 2010 23:15:17 +0100

jcc (2.6-1) unstable; urgency=low

  * New upstream release.
  * Fix typo in README.Debian.
  * Refresh avoid-ftbfs-unknown-archs.patch.
  * Update Standards-Version to 3.9.0.

 -- Ludovico Cavedon <cavedon@debian.org>  Sun, 18 Jul 2010 11:16:04 +0200

jcc (2.5.1-1) unstable; urgency=low

  * New upstream release.
  * Update to Standards-Version 3.8.4.
  * Add support for SH4 architecture. Thanks to Nobuhiro Iwamatsu
    (closes: 571059).

 -- Ludovico Cavedon <cavedon@debian.org>  Thu, 18 Mar 2010 23:20:13 -0700

jcc (2.5-3) unstable; urgency=low

  * Merge patch from Ubuntu by Onkar Shinde:
    - When CPU type is i686, use the JRE lib directory corresponding to i386.
      Fixes FTBFS on lpia.

 -- Ludovico Cavedon <cavedon@debian.org>  Sun, 31 Jan 2010 18:00:47 -0800

jcc (2.5-2) unstable; urgency=low

  * Update avoid-ftbfs-unknown-archs.patch to work around bug #565367.

 -- Ludovico Cavedon <cavedon@debian.org>  Thu, 14 Jan 2010 22:14:27 -0800

jcc (2.5-1) unstable; urgency=low

  * Package takeover because of MIA maintainer.
  * Add avoid-ftbfs-unknown-archs.patch to avoid FTBFS on many archs.
  * Add Vcs-* headers.
  * Update copyright file to DEP-5.
  * Update to Standards-Version 3.8.3 and debhelper compat 7.
  * Update to source package version 3.0 (quilt).

 -- Ludovico Cavedon <cavedon@debian.org>  Tue, 12 Jan 2010 01:15:19 -0800

jcc (2.5-0.1) unstable; urgency=low

  * Non-maintainer upload (closes: 562405).
  * New upstream release (closes: 533486).
  * Add watch file.
  * Depend on python-all-dev so it will be built against all supported python
    versions.
  * Disable shared mode, as it requires patched setuptools (and lower depends
    on python-setuptools version).
  * Fix separators in JCC_LFLAGS.
  * Add depends on ${misc:Depends} required for debhelper.
  * Remove debina/pycompat and add X.-Python-Version control fileds.
  * Add lintian override for libjava.so from openjdk, until bug #562622 is
    solved.
  * Update copyright file (new Apache 2 license).

 -- Ludovico Cavedon <cavedon@debian.org>  Sat, 26 Dec 2009 18:50:19 +0100

jcc (1.9-8) unstable; urgency=low

  * Add ${python:Depends} (closes: 497131)

 -- Jeff Breidenbach <jab@debian.org>  Sat, 30 Aug 2008 18:31:31 -0700

jcc (1.9-7.1) unstable; urgency=low

  * Non-maintainer upload.
  * use -fdollars-in-identifiers to allow build on arm*. (closes: 494690)

 -- Riku Voipio <riku.voipio@iki.fi>  Fri, 22 Aug 2008 22:45:46 +0300

jcc (1.9-7) unstable; urgency=low

  * Fix important debian/changelog typo

 -- Jeff Breidenbach <jab@debian.org>  Sun, 10 Aug 2008 11:29:55 -0700

jcc (1.9-6) unstable; urgency=low

  * Fix powerpc build problem (closes: 494496)

 -- Jeff Breidenbach <jab@debian.org>  Sun, 10 Aug 2008 10:45:47 -0700

jcc (1.9-5) unstable; urgency=low

  * fix build on non-AMD64 (closes: 493992)

 -- Jeff Breidenbach <jab@debian.org>  Thu, 07 Aug 2008 21:54:09 -0700

jcc (1.9-4) unstable; urgency=low

  * fix a dependency bug

 -- Jeff Breidenbach <jab@debian.org>  Wed, 23 Jul 2008 12:33:40 -0700

jcc (1.9-3) unstable; urgency=low

  * Move to main using OpenJDK

 -- Jeff Breidenbach <jab@debian.org>  Tue, 22 Jul 2008 21:20:20 -0700

jcc (1.9-2) unstable; urgency=low

  * Add a file to /etc/ld.so.conf.d

 -- Jeff Breidenbach <jab@debian.org>  Mon, 02 Jun 2008 16:24:31 -0700

jcc (1.9-1) unstable; urgency=low

  * Initial release.

 -- Jeff Breidenbach <jab@debian.org>  Sun, 27 Apr 2008 21:33:03 -0700
